import mongoose from 'mongoose';
import convertor from 'steam-id-convertor';
import { LoggingDB } from '../schemas/loggingSchema';

export const getLeagueListing = (da, TournamentsDB) =>
{
    da.getLeagueListing().then((result) => {
        const LeagueListing = result.result.leagues
        LeagueListing.map((list)=>{
            const leagueName = list.name.substr(11, list.name.length ).replace(/_/g, ' ');
            const leagueDescription = list.description.replace('#DOTA_Item_Desc_', '').replace(/_/g, ' ');

            const bdData = {
                name: leagueName,
                leagueid: list.leagueid,
                description: leagueDescription,
                tournament_url: list.tournament_url,
                itemdef: list.itemdef
            }

            searchSaveDB(TournamentsDB, {leagueid: list.leagueid}, bdData)
            .then( (result) => {
                logging ( 'Success', '23: getLeagueListing > searchSaveDB', result )
                console.log('result', result );
            })
            .catch((error) => {
                logging ( 'Error', '27: getLeagueListing > searchSaveDB', error )
                console.log('error', error );
            })
        })

    }, (errorResponseStatusText) => {
        logging ( 'Error', '34: getLeagueListing', errorResponseStatusText )
        console.log(errorResponseStatusText);
    });
}

export const getLiveLeagueGames = (da, playerDB, TournamentsDB, matchDB, teamDB, Dota2) => {

    da.getLiveLeagueGames()
    .then( ( result ) => {

        const liveLeagueGames = result.result.games;

        liveLeagueGames.map( ( list ) => {
            if ( list.league_tier > 1  && list.dire_team &&  list.radiant_team){
                // saving/searching teams in teamDB
                let dire_teamName = ''
                let radiant_teamName = ''

                const radiant_team = list.radiant_team
                const dire_team = list.dire_team

                console.log( "list.radiant_team", list.radiant_team)
                console.log( "list.dire_team", list.dire_team)
                console.log( "list", list )



                try {
                    radiant_teamName = radiant_team.team_name
                    dire_teamName =  dire_team.team_name
                }
                catch(err) {

                    console.log( "err ", err )
                }

                const DBteam0 = {
                    name: radiant_teamName,
                    steamId: radiant_team.team_id,
                    players : [],
                    matches: list.match_id,
                    tournaments: list.league_id
                }

                const DBteam1 = {
                    name: dire_teamName,
                    steamId: dire_team.team_id,
                    players : [],
                    matches: list.match_id,
                    tournaments: list.league_id,
                }

                const Teams = [ dire_team.team_id, radiant_team.team_id]
                // saving/searching players in playerDB
                const matchPlayes = list.players
                const Players = []

                matchPlayes.map( (playerss) => {

                    if ( playerss.team <= 1 ){

                        Players.push( playerss.account_id )

                        if ( playerss.team === 0 ){

                            DBteam0.players.push( { accountId : playerss.account_id } )
                        }
                        else
                        {
                            DBteam1.players.push( { accountId : playerss.account_id } )
                        }

                        const steamId = convertor.to64(playerss.account_id)

                        Dota2.requestPlayerInfo(playerss.account_id);
                        Dota2.on("playerInfoData", function(playerInfo) {

                            playerss.team_id = playerss.team === 0 ? [ radiant_team.team_id ] : [ dire_team.team_id ]
                            playerss.team_name = playerss.team === 0 ? [ radiant_teamName ] : [ dire_teamName ]

                            console.log("playerss.team_name", playerss.team_name)

                            const Player= {
                                nickName: playerInfo.name
                            ,   accountId: playerss.account_id
                            ,   steamId: steamId
                            ,   position: playerInfo.fantasy_role
                            ,   activeTeam: playerss.team_name
                            ,   matches: list.match_id
                            ,   teams: playerss.team_id
                            ,   tournaments: list.league_id
                            }

                            saveUpdated (playerDB, { accountId: playerss.account_id }, Player, ['matches', 'teams', 'tournaments'] )
                            .then( ( result ) => {
                                logging ( 'Success', '110: getLiveLeagueGames > requestPlayerInfo > saveUpdated', result )
                                console.log('saveUpdated playerDB result >', result );
                            })
                            .catch( ( error ) => {
                                logging ( 'Error', '114: getLiveLeagueGames > requestPlayerInfo > saveUpdated', error )
                                console.log('saveUpdated playerDB error >', error );
                            })
                        });
                    }
                })

                da.getTeamInfoByTeamID ( { start_at_team_id : DBteam0.steamId, teams_requested: 1 } )
                .then( ( result ) => {
                    console.log( "GetTeamInfoByTeamID DBteam0 result >",  result )
                    DBteam0.name = result.result.teams[0].name
                    DBteam0.teamTag = result.result.teams[0].tag
                    // merge leugue ID's form getTeamInfoByTeamID with current.
                    DBteam0.tournaments = mergeTeamInfoLeagueId( result.result.teams[0], DBteam0.tournaments )
                    console.log( "mergeTeamInfoLeagueId DBteam0 result >",  DBteam0.tournaments )


                    saveUpdated (teamDB, { steamId : DBteam0.steamId }, DBteam0, ['matches', 'players', 'tournaments'] )
                    .then( ( result ) => {
                        logging ( 'Succes', '133: getLiveLeagueGames > getTeamInfoByTeamID > saveUpdated', result )
                        console.log('saveUpdated DBteam0 result >', result );
                    })
                    .catch( ( error ) => {
                        logging ( 'Error', '137: getLiveLeagueGames > getTeamInfoByTeamID > saveUpdated', error )
                        console.log('saveUpdated DBteam0 error >', error );
                    })

                })
                .catch( ( error ) => {
                    logging ( 'Error', '143: getLiveLeagueGames > getTeamInfoByTeamID', error )
                    console.log( "GetTeamInfoByTeamID ERROR >",  error )
                })

                da.getTeamInfoByTeamID ( { start_at_team_id : DBteam1.steamId, teams_requested: 1 } )
                .then( ( result ) => {
                    console.log( "GetTeamInfoByTeamID DBteam1 result >",  result )
                    DBteam1.name = result.result.teams[0].name
                    DBteam1.teamTag = result.result.teams[0].tag
                    // merge leugue ID's form getTeamInfoByTeamID with current.
                    DBteam1.tournaments = mergeTeamInfoLeagueId( result.result.teams[0], DBteam1.tournaments )
                    console.log( "mergeTeamInfoLeagueId DBteam1 result >",  DBteam1.tournaments )


                    saveUpdated (teamDB, { steamId : DBteam1.steamId }, DBteam1, ['matches', 'players', 'tournaments'] )
                    .then( ( result ) => {
                        logging ( 'Succes', '159: getLiveLeagueGames > getTeamInfoByTeamID > saveUpdated', result )
                        console.log('saveUpdated DBteam1 result >', result );
                    })
                    .catch( ( error ) => {
                        logging ( 'Error', '163: getLiveLeagueGames > getTeamInfoByTeamID > saveUpdated', error )
                        console.log('saveUpdated DBteam1 error >', error );
                    })

                })
                .catch( ( error ) => {
                    logging ( 'Error', '169: getLiveLeagueGames > getTeamInfoByTeamID', error )
                    console.log( "GetTeamInfoByTeamID ERROR >",  error )
                })


                // make the match input
                const Duration = list.scoreboard.duration

                const matchDetails = [ list.scoreboard ]

                const MatchInfo= {
                    dateUTC: new Date().toISOString(),
                    match_id: list.match_id,
                    league_id: list.league_id,
                    matchDetails : matchDetails,
                    players : Players,
                    teamPlayers : {
                        team0 : DBteam0,
                        team1 : DBteam1
                    },
                    teams : Teams
                }

                saveUpdated ( matchDB, { match_id : list.match_id }, MatchInfo, ['matchDetails'] )
                .then( ( result ) => {
                    logging ( 'Succes', '194: getLiveLeagueGames > save Match Info > saveUpdated', result )
                    console.log('saveUpdated matchDB result >', result );
                })
                .catch( ( error ) => {
                    logging ( 'Error', '198: getLiveLeagueGames > save Match Info > saveUpdated', error )
                    console.log('saveUpdated matchDB error >', error );
                })

                // update tournament info
                const TournamentInfo= {
                    matches: list.match_id,
                    players : Players,
                    teams : Teams
                }

                saveUpdated (TournamentsDB, { leagueid : list.league_id }, TournamentInfo, ['matches', 'players', 'teams'] )
                .then( ( result ) => {
                    logging ( 'Succes', '194: getLiveLeagueGames > save tournament Info > saveUpdated', result )
                    console.log('saveUpdated TournamentsDB result >', result );
                })
                .catch( ( error ) => {
                    logging ( 'Error', '215: getLiveLeagueGames > save tournament Info > saveUpdated', error )
                    console.log('saveUpdated TournamentsDB error >', error );
                })
            }
        })

    }, (errorResponseStatusText) => {
        logging ( 'Error', '222: getLiveLeagueGames', errorResponseStatusText )
        console.log('getLiveLeagueGames ERROR >',errorResponseStatusText);
    });
}

const dbLookUp = ( db, newQuery ) => {

    return new Promise( ( resolve, reject ) => {
        let QueryTest = db.find(
            { $or : newQuery },
            { score : { $meta: "textScore" } }
        )
        .sort({ score : { $meta : 'textScore' } })

        assert.equal(QueryTest.exec().constructor, require('bluebird'));

        QueryTest.then( (result) => {

            resolve(result);
        })
        QueryTest.catch( (error) => {

            reject(error);
        })
    })
}

const saveEntry = (db, data) => {
    const newEntry = new db ({
        ...data
    });

    newEntry.id = db._id;
    return new Promise((resolve, reject) => {
        newEntry.save(function (err) {
            err ? reject(err) : resolve(newEntry)
        })
    })
}

const updateEntry = (ID, input) => {

    return new Promise((resolve, reject) => {

        DB.findByIdAndUpdate(ID, { $set: { ...input }}, { new: true }, function (err, player) {

            err ? reject(err) : resolve(player)
        })
    })
}

const searchSaveDB = ( db, query, dataSave, edit = false ) => {

    return new Promise( ( resolve, reject ) => {

        dbLookUp( db, query )
        .then( (result) => {

            resolve( {
                result: result
            ,   status: "FOUND"
            } );

        })
        .catch( (error) => {

            saveEntry( db, dataSave )
            .then( (result) => {

                resolve( {
                    result: result
                ,   status: "SAVED"
                } );
            })
            .catch( (error) => {

                reject( {
                    result: error
                ,   status: "ERROR"
                } )
            })
        })
    })
}

const saveUpdated = (DB, SearchQ, saveData, columEdit ) => {

    return new Promise( ( resolve, reject ) => {

        searchSaveDB( DB, SearchQ, saveData, true )
        .then( (result) => {

            if ( result.status === "FOUND" ) {

                let entryEdit = result.result[0]

                entryEdit = mergeEntries( columEdit, entryEdit, saveData )

                updateEntry( entryEdit )
                .then( (result) => {
                    resolve( result )
                })
                .catch( (error) => {
                    reject( {
                        result: error
                    ,   status: "ERROR saveUpdated > searchSaveDB > updateEntry"
                    })
                })
            }
            else {
                resolve( result )
            }
        })
        .catch( (error) => {
            reject( {
                result: error
            ,   status: "ERROR saveUpdated > searchSaveDB"
            } )
        })
    })
}

const mergeEntries = (columEdit, entryDB, newInfo ) => {

    for (var i = columEdit.length - 1; i >= 0; i--) {

        if ( Array.isArray( entryDB[columEdit[i]] ) ){

            entryDB[columEdit[i]].push( newInfo[columEdit[i]] )
        }
        else {
            entryDB[columEdit[i]] = []
            entryDB[columEdit[i]].push( newInfo[columEdit[i]] )
        }
    }
    return entryDB
}

const mergeTeamInfoLeagueId = ( TeamInfo, league_id  ) =>
{
    let tournaments = []
    tournaments.push( league_id )
    Object.keys(TeamInfo).map( (key, index) => {
        const leagueid = TeamInfo[key]

        if ( key.indexOf("league_id_") !== -1 && leagueid !== league_id)
        {
            tournaments.push( leagueid )
        }
    })

    tournaments.sort( (a, b) => { return a-b } )

    return tournaments
}

const turnToString = ( data ) =>
{
    if ( typeof data === 'object' )
    {
        data = JSON.stringify( data )
    }
    else if ( typeof data === 'string' )
    {
        data = String( data )
    }

    return data;
}

const logging = ( type, note, text ) => {

    text = turnToString ( text )
    note = turnToString ( note )

    const bdData = {
        type,
        modual: "dota2api",
        note,
        text
    }

    saveEntry( LoggingDB, bdData )
    .then( (result) => {

        console.log("logging SaveEntry > result >", result );
    })
    .catch( (error) => {

       console.log("logging SaveEntry > error >", error )
    })
}