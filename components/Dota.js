import steam from 'steam'
import util from 'util'
import fs from 'fs'
import crypto from 'crypto'
import dota2 from 'dota2'


const steamClient = new steam.SteamClient(),
steamUser = new steam.SteamUser(steamClient),
steamFriends = new steam.SteamFriends(steamClient);
export const Dota2 = new dota2.Dota2Client(steamClient, true);

global.config = {
    steam_name: 'esportsscores',
    steam_user: 'esportsscores',
    steam_pass: 'k1ll3rST#@M'
}


/* Steam logic */
const onSteamLogOn = function onSteamLogOn(logonResp) {
    if (logonResp.eresult == steam.EResult.OK) {
        steamFriends.setPersonaState(steam.EPersonaState.Busy); // to display your steamClient's status as "Online"
        //steamFriends.setPersonaName(global.config.steam_name); // to change its nickname
        util.log("Logged on.");
        Dota2.launch();

    }
}

const onSteamServers = function onSteamServers(servers) {
    util.log("Received servers.");
    fs.writeFile('servers', JSON.stringify(servers), (err)=>{
        if (err) {if (this.debug) util.log("Error writing ");}
        else {if (this.debug) util.log("");}
    });
}

const onSteamLogOff = function onSteamLogOff(eresult) {
    util.log("Logged off from Steam. >> ". eresult);
}

const onSteamError = function onSteamError(error) {
    util.log("Connection closed by server: "+error);
};

steamUser.on('updateMachineAuth', function(sentry, callback) {
    var hashedSentry = crypto.createHash('sha1').update(sentry.bytes).digest();
    fs.writeFileSync('sentry', hashedSentry)
    util.log("sentryfile saved");
    callback({
        sha_file: hashedSentry
    });
});


// Login, only passing authCode if it exists
const logOnDetails = {
    "account_name": global.config.steam_user,
    "password": global.config.steam_pass,
};
if (global.config.steam_guard_code) logOnDetails.auth_code = global.config.steam_guard_code;
if (global.config.two_factor_code) logOnDetails.two_factor_code = global.config.two_factor_code;

try {
    var sentry = fs.readFileSync('sentry');
    if (sentry.length) logOnDetails.sha_sentryfile = sentry;
} catch (beef) {
    util.log("Cannot load the sentry. " + beef);
}

steamClient.connect();
steamClient.on('connected', function() {
    console.log('Step1');
    steamUser.logOn(logOnDetails);
});

steamClient.on('logOnResponse', onSteamLogOn);
steamClient.on('loggedOff', onSteamLogOff);
steamClient.on('error', onSteamError);
steamClient.on('servers', onSteamServers);