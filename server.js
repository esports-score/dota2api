import fs from 'fs'

import Dota2Api from 'dota2-api'
import request from 'request'

import mongoose, { Schema } from 'mongoose';
import { MatchesDB } from './schemas/matchesSchema';
import { PlayerDB } from './schemas/playersSchema';
import { TeamsDB } from './schemas/teamsSchema';
import { TournamentsDB } from './schemas/tournamentsSchema';

import { getLeagueListing,  getLiveLeagueGames, test } from './components/functions';

import dota2 from 'dota2'
import { Dota2 } from './components/Dota';

const COMPOSE_URI_DEFAULT = 'mongodb://localhost:27017/dota';

mongoose.Promise = require('bluebird');
mongoose.connect(COMPOSE_URI_DEFAULT, {
  useMongoClient: true
})
.then( ( db ) => {
  console.log('mongo connected')
  //console.log(db)
})
.catch( ( error ) => {
  console.error(error)
})

const da = Dota2Api.create('16CE6ED4010D84B20B0CADF2DF7BEF0B');

let intervalTime_getLeagueListing = 1000 * 60 * 60 * 12; //12 hour interval
let intervalTime_getLiveLeagueGames = 1000 * 60 * 10; //10 min interval

Dota2.on("ready", function() {
    console.log("Node-dota2 ready.");


    // Dota2.requestMatchDetails(3368158118, function(err, response) {
    //     if (err) throw err;
    //     console.log(JSON.stringify( response ));
    // });

    //Dota2.requestLeagueInfo()

    // Dota2.requestLeaguesInMonth( (new Date()).getMonth() , (new Date()).getFullYear(), 2, function(err, response) {
    //     console.log('requestLeaguesInMonth ERROR' , err );

    //     console.log('requestLeaguesInMonth response' , response );
    //     console.log(JSON.stringify( response ));

    // });

    // const interval_getLeagueListing = setInterval( () => {
    //     getLeagueListing(da, TournamentsDB )
    // }, intervalTime_getLeagueListing );

    const interval_getLiveLeagueGames = setInterval( () => {
        getLiveLeagueGames( da, PlayerDB, TournamentsDB, MatchesDB, TeamsDB, Dota2 )
    }, intervalTime_getLiveLeagueGames );

    //getLeagueListing(da, TournamentsDB )
    getLiveLeagueGames( da, PlayerDB, TournamentsDB, MatchesDB, TeamsDB, Dota2 )
});

Dota2.on("unready", function onUnready() {
    console.log("Node-dota2 unready.");
});

Dota2.on("chatMessage", function(channel, personaName, message) {
    // util.log([channel, personaName, message].join(", "));
});

Dota2.on("guildInvite", function(guildId, guildName, inviter) {
    // Dota2.setGuildAccountRole(guildId, 75028261, 3);
});

Dota2.on("unhandled", function(kMsg) {
    console.log("UNHANDLED MESSAGE " , kMsg );
    console.log("UNHANDLED MESSAGE " , dota2._getMessageName(kMsg));
});

Dota2.on("sourceTVGamesData", function(sourceTVGamesData) {

    console.log("sourceTVGamesResponse " , sourceTVGamesData);
});

Dota2.on("leagueData", function(leagueData) {
    leagueData.map( ( list, index ) => {
        if ( list.league_id === 5401 ){
            console.log("leagueData >"+index , list );
        }
    })
});

Dota2.on("leaguesInMonthData", function(leaguesInMonthData) {

    console.log("leaguesInMonthData " , leaguesInMonthData);
});