import mongoose, { Schema } from 'mongoose';
import timestamps from 'mongoose-timestamp';
import assert from 'assert';

// Mongoose Schema definition
const MatchesSchema = new Schema({
  dateUTC: String,
  ended: Boolean,
  lenght: String,
  matchDetails: Schema.Types.Mixed,
  match_id: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  league_id: String,
  players: Schema.Types.Mixed,
  teams: Schema.Types.Mixed,
  teamPlayers: Schema.Types.Mixed,
  winner: String
});
MatchesSchema.plugin(timestamps);
export const MatchesDB = mongoose.model( 'Matches', MatchesSchema );